#include <string>
using namespace std;
#ifndef PAGE_H
#define PAGE_H
/**
 * @brief The Page class This has details about a page including url, and various statistics. Includes ability to print statitistics/data
 * Links
 */
class Page{
private:
    string url;
    int visits;
    int usedLinks;
    Page* links[3];

public:
    /**
 * @brief Page empty page, all values set to null or zero.
 */
    Page();
    /**
 * @brief Page same values as the empty page
 * @param urlvalue replaces url in empty page with this
 */
    Page(string urlvalue);
    /**
 * @brief getNumLinks return how many of the 3 possible links are full
 * @return
 */
    int getNumLinks();
    /**
 * @brief addLink add url to the next empty slot
 * @param Other the page we are adding to the slot
 */
    void addLink(Page* Other);
    /**
 * @brief getURL returns the url of the page we are looking at
 * @return value set by the constructor when object is created
 */
    string getURL();
    /**
 * @brief getRandomLink select an open slot randomly
 * @return valid slot if non then returns nullptr
 */
    Page* getRandomLink();
    /**
 * @brief visit add 1 to the number of visits, for the page objects they all start at zero.
 */
    void visit();
    /**
 * @brief getNumVisits gets the number of visits from the object
 * @return this number is returned as an int to the program
 */
    int getNumVisits();
    /**
 * @brief print Calculates the percentage of page visits a site has. Then Nicely Prints it out
 */
    void print();

};

#endif // PAGE_H
