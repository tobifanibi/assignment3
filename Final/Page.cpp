#include "Page.h"
#include <iostream>
Page::Page()
{
    url="";
    visits=0;
    usedLinks=0;
    for(int i=0;i<3;i++)
        links[i]=nullptr;
}

Page::Page(string urlvalue)
{
    url=urlvalue;
    visits=0;
    usedLinks=0;
    for(int i=0;i<3;i++)
        links[i]=nullptr;
}

int Page::getNumLinks()
{
    return usedLinks;
}

void Page::addLink(Page* Other){
    int currentnumber=this->getNumLinks();
    links[currentnumber]=Other;
    usedLinks++;
}
string Page::getURL()
{
    return url;
}
//We might have to use heap because we can't use a local pointer that is destroyed
//When we leave the function
//Edit seems to work because what it is pointed to is part of the class
Page* Page::getRandomLink()
{
    int max=this->getNumLinks();
    if(max==0)
        return nullptr;
    int random=rand()%max;
    Page* mypage;
    mypage=links[random];
    return mypage;
}
void Page::visit()
{
    visits++;
}

int Page::getNumVisits()
{
    return visits;
}

void Page::print()
{
    string theurl=this->getURL();
    int remove=theurl.find(':');
    theurl.erase(0,remove+3);
    double myvisits=this->getNumVisits();
    myvisits=(myvisits/10000)*100;
    cout<<theurl<<" "<<myvisits<<"%"<<endl;
}
